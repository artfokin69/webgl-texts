import { Group, Scene, Clock, ShaderMaterial, DoubleSide, Vector4, Vector2, Mesh, Material } from "three";
import * as _THREE from 'three';
import extendMaterial from './plugins/extendMaterial';
import { GLTF } from 'three/examples/jsm/loaders/GLTFLoader';
import autoBind from 'auto-bind';
import {loadScene} from './helpers';
import materialVertexShader from '../assets/shaders/material.vert';
import materialFragmentShader from '../assets/shaders/material.frag';
import {GUI} from 'dat.gui';

const THREE = extendMaterial(_THREE);

export default class Model{
  gltf: GLTF;
  figureMesh: Mesh;
  loaded = false;
  isInit = false;
  shaderMaterial: ShaderMaterial;
  constructor(){
    autoBind(this);
  }
  async load(){
    this.gltf = await loadScene('./christmas_deer/scene.gltf');
    this.loaded = true;
  }
  init(scene: Scene){
    this.makeMaterial();
    this.makeFigure(scene);
    this.extendFigureMaterial();
    this.isInit = true;
  }
  makeMaterial(){
    this.shaderMaterial = new ShaderMaterial({
      extensions: {
        derivatives: true,
      },
      side: DoubleSide,
      uniforms: {
        time: { value: 0 },
        resolution: { value: new Vector4() },
        uvRate1: {
          value: new Vector2(1, 1)
        }
      },
      // wireframe: true,
      // transparent: true,
      vertexShader: materialVertexShader,
      fragmentShader: materialFragmentShader
    });
  }
  makeFigure(scene: Scene){
    this.gltf.scene.traverse((node: Mesh)=>{
      if(node.isMesh){
        this.figureMesh = node;
      }
    })
    this.figureMesh.scale.set(5,5,5);
    this.figureMesh.geometry.rotateX(-Math.PI / 2);
    this.figureMesh.geometry.center();
    this.figureMesh.position.y = 5;
    scene.add(this.figureMesh);
  }
  
  extendFigureMaterial(){
     //https://github.com/Fyrestar/THREE.extendMaterial
     const patchMaterial = (material: Material)=>{
      material.onBeforeCompile = shader=>{
        shader.uniforms.uTime = {
          value: 0
        }
        THREE.patchShader(shader, {
          vertexHeader: 'uniform float uTime;', 
          vertex: {
            '#include <fog_vertex>': `
              vec3 pos = position; 
              pos.x += 1. * sin(uTime); 
              pos.y += (1. + sin(uTime * 0.5)) * 0.5;
              pos.z += 3. * sin(uTime * 2.);
              gl_Position = projectionMatrix * modelViewMatrix * vec4( pos, 1.0 );
            `
          },
          // fragment: {
          //   '@gl_FragColor = vec4( outgoingLight, diffuseColor.a );' : `gl_FragColor = vec4( 1., 0., 0., 1. );`
          // },
        });
        material.userData.shader = shader;
      }
    }
    this.figureMesh.traverse((node: Mesh)=>{
      if(node.isMesh){
        node.material = Array.isArray(node.material) ? node.material[0] : node.material;
        patchMaterial(node.material)
      }
    })
  }
  
  onResize(w:number,h:number){
    
  }
  onRender(clock: Clock){
    const time = clock.getElapsedTime();
    this.shaderMaterial.uniforms.time.value = time;
    // update time in extended model
    this.figureMesh.traverse((node: Mesh)=>{
      if(node.isMesh && node.material){
        const shader = node.material.userData.shader;
        if(shader){ 
          shader.uniforms.uTime.value = time;
        }
      }
    })
  }
  setupGui(gui: GUI){
  }
}