uniform float time;
varying vec2 vUv;
varying vec3 vPosition;
uniform vec2 pixels;

void main() {
  vUv = uv;
  vec3 pos = position;
  pos.x += 10.;
  gl_Position = projectionMatrix * modelViewMatrix * vec4( pos, 1.0 );
}