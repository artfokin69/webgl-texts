import {WebGLRenderer, Scene, PerspectiveCamera, CameraHelper, Color, sRGBEncoding, PCFSoftShadowMap, BufferGeometry,Sphere, Box3, Texture, BufferAttribute, Clock, BoxBufferGeometry, MeshPhongMaterial, Mesh, AxesHelper, Light, PlaneBufferGeometry, MathUtils, MeshBasicMaterial, Fog, AudioListener, Audio, OrthographicCamera, DoubleSide, BoxHelper, WebGLRenderTarget, SubtractiveBlending, MultiplyBlending, ShaderMaterial, MeshStandardMaterial} from 'three';
(window as any).THREE = { ...(window as any).THREE, BufferGeometry, Sphere, Box3, Texture, Color, BufferAttribute };
import { RenderPass, ShaderPass, EffectComposer, ClearPass } from 'postprocessing';
import { OrbitControls } from 'three/examples/jsm/controls/OrbitControls.js';
import Stats from 'three/examples/jsm/libs/stats.module.js';
import autoBind from 'auto-bind';
import createGeometry from 'three-bmfont-text';
import fontPath from '../assets/fonts/font.fnt';
import fontTexture from '../assets/fonts/font-texture.png';
import {GUI} from 'dat.gui';
import LightGroup from './LightGroup';
import { loadTexture, loadFont } from './helpers';


const BlendShader = {
	uniforms: {
		inputBuffer: {value: null},
		sceneTexture: {value: null}
	},

	vertexShader: `
		varying vec2 vUv;

		void main() {
			vUv = uv;
			gl_Position = projectionMatrix * modelViewMatrix * vec4(position, 1.0);
		}
	`,

	fragmentShader: `
		uniform sampler2D sceneTexture;
		uniform sampler2D inputBuffer;

		varying vec2 vUv;

		void main() {
			vec4 sceneTexel = texture2D(sceneTexture, vUv);
			vec4 inputTexel = texture2D(inputBuffer, vUv);
			vec3 diff = abs(sceneTexel.rgb - inputTexel.rgb);

			gl_FragColor = vec4(diff, 1.0);
		}
	`
}

export default class App{
  container: HTMLDivElement;
  renderer: WebGLRenderer;
  scene: Scene;
  sceneTexts: Scene;
  camera: PerspectiveCamera;
  cameraTexts: OrthographicCamera;
  renderTarget: WebGLRenderTarget;
  composer: EffectComposer;

  cameraHelper: CameraHelper;
  orbitControl: OrbitControls;
  clock: Clock;
  stats: Stats;
  cube: Mesh;
  lightGroup: LightGroup;
  logCameraPosition = false;
  listener: AudioListener;
  
  constructor(){
    autoBind(this);
    this.initThree();
    this.lightGroup = new LightGroup(this.scene);
    this.initPostprocessing();
    this.setupSceneHelpers();
    this.addCube();
    this.setupGUI();
    this.render();
    window.addEventListener('resize', this.onResize);

    (async ()=>{
      const [font, texture] = await Promise.all([loadFont(fontPath), loadTexture(fontTexture)]);
      this.addText("Saharov \n Fokin", font, texture);
    })()
  }

  initThree(){
    this.container = document.querySelector('div#container');
    this.scene = new Scene();
    this.scene.background = new Color(0x61cac6);

    this.sceneTexts = new Scene();

    this.camera = new PerspectiveCamera(45, window.innerWidth / window.innerHeight, 1, 1000);
    this.camera.position.set(7, 40, 80);
    const canvas = document.createElement('canvas');
    const context = canvas.getContext( 'webgl', { alpha: false } );

    this.cameraTexts = new OrthographicCamera( window.innerWidth / - 2, window.innerWidth / 2, window.innerHeight / 2, window.innerHeight / - 2, 1, 1000 );
    this.cameraTexts.position.set(0, 0, 1);
    this.scene.background
    this.renderer = new WebGLRenderer( { canvas, context, antialias: true } );
    this.renderer.setPixelRatio( window.devicePixelRatio );
    this.renderer.setSize( window.innerWidth, window.innerHeight );
    this.renderer.outputEncoding = sRGBEncoding;
    this.renderer.shadowMap.enabled = true;
    this.renderer.shadowMap.type = PCFSoftShadowMap;
    this.renderer.autoClear = false

    this.renderTarget = new WebGLRenderTarget(window.innerWidth, window.innerHeight);

    this.container.appendChild(this.renderer.domElement);
  }
  initPostprocessing(){
		const renderScenePass = new RenderPass(this.scene, this.camera)
		renderScenePass.clear = false
		renderScenePass.renderToScreen = false

    const renderTextsPass = new RenderPass(this.sceneTexts, this.cameraTexts)
		renderTextsPass.clear = false
		renderTextsPass.renderToScreen = false

		const shaderMaterial = new ShaderMaterial(BlendShader)
		shaderMaterial.uniforms.sceneTexture.value = this.renderTarget.texture

		const shaderPass = new ShaderPass(shaderMaterial)
		shaderPass.renderToScreen = true

		// Composer and loading
		this.composer = new EffectComposer(this.renderer)
		this.composer.addPass(new ClearPass)
		this.composer.addPass(renderTextsPass)
		this.composer.addPass(shaderPass)
  }
  addText(text: string, font: any, texture: Texture){
    const geometry = createGeometry({
      width: 300,
      align: 'left',
      font,
      flipY: texture.flipY,
      text: text
    });
    
    const material = new MeshStandardMaterial({
      color: 0x0f0f0f0,
      map: texture,
      side: DoubleSide,
      transparent: true
    })

    const mesh = new Mesh(geometry, material)
    mesh.rotateY(MathUtils.degToRad(180));
    mesh.position.set(0,0,0);
    mesh.scale.multiplyScalar(-1)
    this.sceneTexts.add(mesh);
    const box = new BoxHelper(mesh, 0xffff00);
    this.sceneTexts.add(box);
    const boundingBox = new Box3();
    mesh.geometry.computeBoundingBox();
    boundingBox.copy( mesh.geometry.boundingBox ).applyMatrix4( mesh.matrixWorld );
    mesh.position.x = -boundingBox.max.x / 2;
    mesh.position.y = -boundingBox.max.y / 2;
    box.update()
  }
  onResize(){
    this.updateCamera();
    this.renderer.setSize( window.innerWidth, window.innerHeight );
  }
  updateCamera(){
    this.camera.aspect = window.innerWidth / window.innerHeight;
    this.camera.updateProjectionMatrix();
  }
  setupSceneHelpers(){
    // this.orbitControl = new OrbitControls(this.camera, this.renderer.domElement);
    this.orbitControl = new OrbitControls(this.camera, document.body);
    this.orbitControl.maxDistance = 170;
    this.clock = new Clock();
    this.stats = new Stats();
    const axesHelper = new AxesHelper( 20 );
    // this.scene.add(axesHelper);
    document.body.appendChild(this.stats.dom);
  }
  addCube(){
    const cubeSize = 1;
    const geometry = new BoxBufferGeometry(cubeSize, cubeSize, cubeSize);
    const material = new MeshPhongMaterial({color: '#ff00ff'});
    const mesh = new Mesh(geometry, material);
    mesh.castShadow = true
    this.cube = mesh;
    this.scene.add(mesh);
  }
  addPlane(){
    const planeSize = window.innerWidth * 0.5;
    const repeats = planeSize / 2;
    const planeGeo = new PlaneBufferGeometry(planeSize, planeSize);
    const planeMat = new MeshPhongMaterial({color: 0x000000})
    const mesh = new Mesh(planeGeo, planeMat);
    mesh.position.x = planeSize/2;
    mesh.receiveShadow = true;
  }
  addFog(){
    // this.scene.fog = new Fog(this.scene.background as Color, 93, 177);
  }
  setupGUI(){
    const gui = new GUI();
    gui.add({['Log_Camera_Position']: false},'Log_Camera_Position').onChange(log=>this.logCameraPosition = log)
    this.lightGroup.setupGui(gui);
    if(this.scene.fog){
      const fogFolder = gui.addFolder('Fog');    
      fogFolder.add(this.scene.fog, 'far', 100, 500, 1);
      fogFolder.add(this.scene.fog, 'near', 0, 300, 1);
    }
  }
  render(){
    requestAnimationFrame(this.render);
    this.cube.rotation.x += 0.01;
    this.cube.rotation.y += 0.01;
    
    if(this.logCameraPosition){
      console.log(this.camera.position);
    }
    this.lightGroup.onRender(this.clock);
    this.camera.updateProjectionMatrix();
    this.stats.update();
    this.orbitControl.update();
    
    
    // get underlay texture
    this.renderer.setRenderTarget(this.renderTarget)
    this.renderer.render(this.scene, this.camera);
    this.renderer.setRenderTarget(null);
    this.composer.render(this.clock.getDelta());
    
  }
}

new App();